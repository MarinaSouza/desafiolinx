// Leitura do objeto JSONP
function start() {
    var s = document.createElement("script");
    s.src = "http://roberval.chaordicsystems.com/challenge/challenge.json?callback=X";
    document.body.appendChild(s);
    pagging();
}

function X(response) {
    insere_referencia(response);
    insere_recomendacoes(response);

}

//Divisão dos atributos do objeto nos elementos do carrosel
function insere_recomendacoes(response) {
    var itens = response.data.recommendation.length;
    var i;
    for (i = 0; i < itens; i++) {
        var parcela = response.data.recommendation[i].price;
        parcela = formata_parcela(parcela);
        document.getElementById(i + 10).src = "http:" + response.data.recommendation[i].imageName;
        if (response.data.recommendation[i].oldPrice == null) {
            document.getElementById(i).innerHTML = response.data.recommendation[i].name + "<p class='preco'> Por: <span class='price'>" + response.data.recommendation[i].price + "</span><p class='preco'> <strong>ou 12x de R$ " + parcela + "</strong> <br> sem juros<p>";
        } else document.getElementById(i).innerHTML = response.data.recommendation[i].name + "<p>De:" + response.data.recommendation[i].oldPrice + " </p><p class='preco'> Por: <span class='price'>" + response.data.recommendation[i].price +"</span><p class='preco'> <strong>ou 12x de R$ " + parcela + "</strong> <br>  sem juros<p>";
        document.getElementById("link" + i).href = "http:" + response.data.reference.item.detailUrl;
        var recommendation = document.getElementById("link" + i);
        recommendation.setAttribute('target', '_blank');
    }
}

//Divisão dos atributos do objeto no elemento referência
function insere_referencia(response) {
    var parcela = response.data.reference.item.price;
    parcela = formata_parcela(parcela);
    document.getElementById("ref").src = "http:" + response.data.reference.item.imageName;
    document.getElementById("ref1").innerHTML = response.data.reference.item.name + "<p class='preco'> Por: <span class='price'>" + response.data.reference.item.price +"</span><p class='preco'> <strong>ou 12x de R$ " + parcela + "</strong><br> sem juros<p>";
    document.getElementById("img_ref").href = "http:" + response.data.reference.item.detailUrl;
    var reference = document.getElementById("img_ref");
    reference.setAttribute('target', '_blank');

}

//Foramatação do valor da parcela
function formata_parcela(parcela) {
    parcela = parcela.slice(2); //Removendo o cifrão
    parcela = parseInt(parcela) / 12;
    parcela = parcela.toFixed(2); //Limitando os digitos ponto flutuante
    parcela = parcela.replace(".", ","); //Substituindo o caracter divisor
    return parcela;
}

function pagging(){
  var ident = 0;
  var slide = 400; //tamanho em pixels em que será movimentado o slide
  var seta_next = document.getElementById('next');
  var seta_prev = document.getElementById('prev');
  var min = document.getElementById('bloco_carrosel').offsetLeft; //Posição inicial do carrosel

  seta_next.onclick = function() {
    if (ident < 4) {
      var pos_atual = document.getElementById('bloco_carrosel').offsetLeft; // Referencial inicial
      var diff = pos_atual - slide; // posição destino
      var pos_px = diff + 'px'; // Formatação da posição destino para leitura na função
      ident++; //incrementador para contabilizar quantas vezes o carrosel movimentou
      document.getElementById("bloco_carrosel").style.marginLeft = pos_px; // Setando novo valor

    }
  };
  seta_prev.onclick = function() {
    if (ident > 0) {
      var pos_atual = document.getElementById('bloco_carrosel').offsetLeft;
      var diff = pos_atual + slide;
      if (diff < min){
      var pos_px = diff + 'px';
      ident--;
      document.getElementById("bloco_carrosel").style.marginLeft = pos_px;
    }else {
      diff = min;
      ident--;
      var pos_px = diff + 'px';
      document.getElementById("bloco_carrosel").style.marginLeft = pos_px;
    }
    }
  };
}
